*** Settings ***
Library   Browser

*** Test Cases ***
Test external endpoint
    New Browser     chromium
    New Page    http://1.1.4.12:3000/
    ${page_source}  Get Page Source
    Should Contain  ${page_source}  Hello World!

Test internal endpoint
    New Browser     chromium
    New Page    http://frontend-service.nja:3000/
    ${page_source}  Get Page Source
    Should Contain  ${page_source}  Hello World!
