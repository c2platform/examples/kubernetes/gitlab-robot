# Run Robot tests on Kubernetes using GitLab

This project demonstrates how we can run [Robot Framework](https://robotframework.org/) tests on  [MicroK8s](https://microk8s.io/) Kubernetes cluster running on `c2d-ks1` using **GitLab Runner** running in the cluster. In this project the **GitLab Runner** is installed in the namespace `nja` where the `nj` "helloworld" application is running. This allows us to create tests that use internal endpoints. For example [test/chrome.robot](./test/chrome.robot) has two tests, one using the internal endpoint, one using the external endpoint.

- [Overview](#overview)
- [Create Kubernetes cluster](#create-kubernetes-cluster)
- [Deploy hellworld app](#deploy-hellworld-app)
- [Install GitLab Runner](#install-gitlab-runner)
- [Run test](#run-test)

## Overview

```plantuml
@startuml crc-remote
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")
'Person(user, "User")

Boundary(local, "local", $type="high-end dev laptop") {
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1")
    Boundary(c2d_ks, "Kubernetes", $type="c2d-ks1") {
        Boundary(c2d_k8s_nja, "nja", $type="namespace") {
            Container(runner, "Gitlab Runner", "")
            Container(nj, "helloworld", "application")
        }
    }
}
Boundary(gitlab, "gitlab.com/c2platform", $type="") {
    Boundary(njx, "examples/kubernetes/gitlab-robot", $type="") {
        Container(repo, "Repository", "git")
        Container(pipeline, "Pipeline", "")
    }
    Boundary(robot_project, "docker/robot", $type="") {
        Container(robot_registry, "Registry", "registry")
    }

}

Rel(engineer, repo, "Push test code", "")
Rel(engineer, c2d_rproxy1, "")
Rel(engineer, pipeline, "Run tests")
Rel(c2d_rproxy1, nj, "frontend-nja.k8s.c2platform.org")
Rel_Left(repo, pipeline, "Trigger", "")
Rel(pipeline, runner, "Test", "stage")
Rel(runner, nj, "Test requests", "robot")
'Rel_Down(user, nj, "", "")
Rel_Left(runner, robot_registry, "Pull robot \nimage", "")
@enduml
```

## Create Kubernetes cluster

Create local Kubernetes cluster on `c2d-ks`.

* [How-to Kubernetes](https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes.md)
* [How-to Kubernetes Dashboard](https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-k8s-dashboard.md) ( optional but recommended)

## Deploy hellworld app

* See project [GitLab GitOps workflow for Kubernetes](https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops)

## Install GitLab Runner

In this project **Shared Runners** are of course turned off. The [.gitlab-ci.yml](./.gitlab-ci.yml) in this project only contains one stage and job, which we want to delegate to Kubernetes running locally which we can do by installing a **GitLab Runner** inside the cluster.

* [How-to Install Gitlab Runner](https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes-gitlab-runner.md)

## Run test

Navigate to [gitlab-robot pipelines](https://gitlab.com/c2platform/examples/kubernetes/gitlab-robot/-/pipelines) and select **Run pipeline** to start the Robot tests.
